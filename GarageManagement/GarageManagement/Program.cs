﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.UI;
using GarageManagement.BL.Car;

namespace GarageManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IUI ui = new GarageUIManager();
                ui.Start();
            }
            catch (Exception e)
            {
                GarageUIManager.PrintError(e.ToString());
                GarageUIManager.PrintExitMessage();
            }
        }
    }
}
