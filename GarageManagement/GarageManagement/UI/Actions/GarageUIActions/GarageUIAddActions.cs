﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Car;
using GarageManagement.BL.Exceptions;
using GarageManagement.BL.Consts;
using GarageManagement.UI.Actions.IActions;

namespace GarageManagement.UI.Actions.GarageUIActions
{
    public class GarageUIAddActions : IAddActions
    {
        private IAdvanceGarage garage;

        public GarageUIAddActions(IAdvanceGarage garage)
        {
            this.garage = garage;
        }
        public void AddCar()
        {
            Console.Write(InputConsts.ENTER_CAR_MODEL_MESSAGE);
            String carModel = Utils.GetString();

            Console.Write(InputConsts.ENTER_CAR_NUMBER_MESSAGE);
            String carNumber = Utils.GetString();

            IActiveCar car = Garage.CreateCar(carNumber, carModel);

            Garage.InsertToGarage(garage, car);

            GarageUIPrintActions.PrintCarResult(UIConsts.CAR_ADDED_MESSAGE, car);
        }
    }
}
