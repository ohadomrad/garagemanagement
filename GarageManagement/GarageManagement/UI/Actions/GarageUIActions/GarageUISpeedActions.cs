﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Car;
using GarageManagement.BL.Exceptions;
using GarageManagement.BL.Consts;
using GarageManagement.UI.Actions.IActions;


namespace GarageManagement.UI.Actions.GarageUIActions
{
    public class GarageUISpeedActions :ISpeedActions
    {

        private IAdvanceGarage garage;

        public GarageUISpeedActions(IAdvanceGarage garage)
        {
            this.garage = garage;
        }
        public void BoostSpeedByOne()
        {
            IActiveCar car = this.garage.GetDefaultCar();
            car.BoostSpeed();

            GarageUIPrintActions.PrintCarResult(UIConsts.CAR_BOOST_MESSAGE, car);
        }

        public void BoostSpeedByConstant()
        {
            try
            {
                Console.Write(InputConsts.ENTER_BOOST_SPEED);
                double speedBoost = Utils.GetDouble();
                IActiveCar car = this.garage.GetDefaultCar();
                car.BoostSpeed(speedBoost);

                GarageUIPrintActions.PrintCarResult(UIConsts.CAR_BOOST_MESSAGE, car);
            }

            catch (FormatException)
            {
                throw new InputException(ExceptionConsts.SPEED_INPUT_ERROR);
            }
        }

        public void DecreaseSpeedByOne()
        {
            IActiveCar car = this.garage.GetDefaultCar();
            car.DecreaseSpeed();

            GarageUIPrintActions.PrintCarResult(UIConsts.CAR_DECREASE_MESSAGE, car);
        }

        public void DecreaseSpeedByConstant()
        {
            try
            {
                Console.Write(InputConsts.ENTER_DECREASE_SPEED);
                double speedDecrease = Utils.GetDouble();

                IActiveCar car = this.garage.GetDefaultCar();
                car.DecreaseSpeed(speedDecrease);

                GarageUIPrintActions.PrintCarResult(UIConsts.CAR_DECREASE_MESSAGE, car);
            }

            catch (FormatException)
            {
                throw new InputException(ExceptionConsts.SPEED_INPUT_ERROR);
            }
        }
    }
}
