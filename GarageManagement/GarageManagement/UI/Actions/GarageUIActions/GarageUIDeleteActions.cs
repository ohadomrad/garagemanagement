﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Car;
using GarageManagement.BL.Exceptions;
using GarageManagement.BL.Consts;
using GarageManagement.UI.Actions.IActions;


namespace GarageManagement.UI.Actions.GarageUIActions
{
    public class GarageUIDeleteActions : IDeleteActions
    {
        private IAdvanceGarage garage;

        public GarageUIDeleteActions(IAdvanceGarage garage)
        {
            this.garage = garage;
        }
        public void DeleteCarByNumber()
        {
            Console.Write(InputConsts.ENTER_CAR_NUMBER_MESSAGE);
            String carNumber = Utils.GetString();
            Car deletedCar = new Car(this.garage.GetCarByCarNumber(carNumber));

            this.garage.DelteCarByNumber(carNumber);

            GarageUIPrintActions.PrintCarResult(UIConsts.DELETE_CAR_MESSAGE, deletedCar);
        }

        public void DeleteDefualtCar()
        {
            Car deletedCar = new Car(this.garage.GetDefaultCar());
            this.garage.DeleteDefaultCar();

            GarageUIPrintActions.PrintCarResult(UIConsts.DELETE_CAR_MESSAGE, deletedCar);
        }
    }
}
