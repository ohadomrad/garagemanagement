﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Car;
using GarageManagement.BL.Exceptions;
using GarageManagement.BL.Consts;
using GarageManagement.UI.Actions.IActions;

namespace GarageManagement.UI.Actions.GarageUIActions
{
    public class GarageUIPrintActions : IPrintActions
    {
        private IAdvanceGarage garage;
        
        public GarageUIPrintActions(IAdvanceGarage garage)
        {
            this.garage = garage;
        }

        public static void PrintCarResult(String message, ICar car)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(message);
            PrintCar(car);
            Console.ForegroundColor = ConsoleColor.Green;
        }

        public static void PrintCar(ICar car)
        {
            Console.WriteLine(car.ToString());
        }

        public void PrintDefaultCar()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            PrintCar(garage.GetDefaultCar());
            Console.ForegroundColor = ConsoleColor.Green;
        }

        public void PrintEqualsToDefault()
        {
            List<IActiveCar> equalsCars = this.garage.GetEqualsToDefault();
            Console.ForegroundColor = ConsoleColor.Blue;
            equalsCars.ForEach(car => PrintCar(car));
            Console.ForegroundColor = ConsoleColor.Green;
        }
        public void PrintCarByIndex()
        {
            try
            {
                Console.Write(InputConsts.ENTER_CAR_INDEX_MESSAGE);
                int index = Utils.GetInt();
                ICar car = this.garage.GetCarByIndex(index);
                PrintCarResult(String.Format(UIConsts.CAR_INDEX_MESSAGE_FORMAT, index), car);
            }
            catch (FormatException)
            {
                throw new InputException(ExceptionConsts.INDEX_INPUT_ERROR);
            }
        }

        public void PrintCarsByModelName()
        {
            String name = "";
            Console.Write(InputConsts.ENTER_CAR_MODEL_MESSAGE);
            name = Utils.GetString();
            List<IActiveCar> cars = Garage.GetCarsByModelName(garage, name);
            Console.ForegroundColor = ConsoleColor.Blue;
            cars.ForEach(car => PrintCar(car));
            Console.ForegroundColor = ConsoleColor.Green;
        }
        public void PrintAllCars()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            this.garage.Cars.ForEach(car => PrintCar(car));
            Console.ForegroundColor = ConsoleColor.Green;
        }
    }
}
