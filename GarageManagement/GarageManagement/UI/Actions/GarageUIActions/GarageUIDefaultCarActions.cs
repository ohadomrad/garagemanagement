﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Car;
using GarageManagement.BL.Exceptions;
using GarageManagement.BL.Consts;
using GarageManagement.UI.Actions.IActions;

namespace GarageManagement.UI.Actions.GarageUIActions
{
    public class GarageUIDefaultCarActions :IDefaultCarActions
    {
        private IAdvanceGarage garage;

        public GarageUIDefaultCarActions(IAdvanceGarage garage)
        {
            this.garage = garage;
        }

        public void SetDefaultCarByIndex()
        {
            Console.Write(InputConsts.ENTER_CAR_INDEX_MESSAGE);
            try
            {
                int index = Utils.GetInt();
                this.garage.SetDefaultCar(index);
            }
            catch (FormatException)
            {
                throw new InputException(ExceptionConsts.INDEX_INPUT_ERROR);
            }
        }

        public void SetDefaultCarByNumber()
        {
            Console.Write(InputConsts.ENTER_CAR_NUMBER_MESSAGE);
            String carNumber = Utils.GetString();
            this.garage.SetDefaultCar(carNumber);
        }
    }
}
