﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.UI.Actions.IActions
{
    public interface IAddActions
    {
        void AddCar();
    }
}
