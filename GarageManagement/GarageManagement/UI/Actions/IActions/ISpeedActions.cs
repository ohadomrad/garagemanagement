﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.UI.Actions.IActions
{
    interface ISpeedActions
    {
        void BoostSpeedByOne();
        void BoostSpeedByConstant();
        void DecreaseSpeedByOne();
        void DecreaseSpeedByConstant();

    }
}
