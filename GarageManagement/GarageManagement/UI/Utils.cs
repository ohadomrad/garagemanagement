﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Exceptions;

namespace GarageManagement.UI
{
    public class Utils
    {
        public static int GetInt()
        {
            return int.Parse(Console.ReadLine()); 
        }

        public static double GetDouble()
        {
            return Double.Parse(Console.ReadLine());
        }

        public static String GetString()
        {
            return Console.ReadLine();
        }
    }
}
