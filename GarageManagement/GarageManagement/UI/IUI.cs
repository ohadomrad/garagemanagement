﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.UI
{
    public interface IUI
    {
        void Start();
        void Menu();
        void HandleClient();
        bool IsClientExit();

    }
}
