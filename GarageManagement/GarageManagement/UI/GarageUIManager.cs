﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Consts;
using GarageManagement.BL.Exceptions;
using GarageManagement.BL.Factory;
using GarageManagement.BL.Car;
using GarageManagement.UI.Actions.GarageUIActions;
using GarageManagement.UI.Actions.IActions;

namespace GarageManagement.UI
{
    class GarageUIManager : IUI
    {
        bool clientExit = false;
        private IMethodsFactory<String, Action> menuActions = new ActionsFactory();
        private IAdvanceGarage garage = new Garage();

        private IPrintActions uiPrintActions;
        private IDeleteActions uiDeleteActions;
        private IAddActions uiAddActions;
        private IDefaultCarActions uiDefualtActions;
        private ISpeedActions uiSpeedActions;


        public GarageUIManager()
        {
            Garage.InsertToGarage(garage, new Car("123", 2020, "KIA"));
            Garage.InsertToGarage(garage, new Car("456", 2019, "KIA"));
            Garage.InsertToGarage(garage, new Car("111", 2015, "TOYOTA"));

            uiPrintActions = new GarageUIPrintActions(garage);
            uiDeleteActions = new GarageUIDeleteActions(garage);
            uiAddActions = new GarageUIAddActions(garage);
            uiDefualtActions = new GarageUIDefaultCarActions(garage);
            uiSpeedActions = new GarageUISpeedActions(garage);

            InitalUIClientActions();
        }

        public void Start()
        {            
            while (!IsClientExit())
            {
                try
                {
                    Menu();
                    HandleClient();
                }
                catch (ChoiceException e)
                {
                    PrintError(e.ToString());
                }
                catch (ManufactureYearException e)
                {
                    PrintError(e.ToString());
                }

                catch (NumCarException e)
                {
                    PrintError(e.ToString());
                }

                catch (DuplicateCarException e)
                {
                    PrintError(e.ToString());
                }
                catch (InvalidCarIndexException e)
                {
                    PrintError(e.ToString());
                }
                catch (SpeedException e)
                {
                    PrintError(e.ToString());
                }

                catch (SpeedConstantException e)
                {
                    PrintError(e.ToString());
                }

                catch (EmptyGarageException e)
                {
                    PrintError(e.ToString());
                }

                catch(CarNumberNotExistsException e)
                {
                    PrintError(e.ToString());
                }

                catch(InputException e)
                {
                    PrintError(e.ToString());
                }

                finally
                {
                    ClearScrean();
                }

            }

            PrintExitMessage();
        }

        public void HandleClient()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            String choice = GetClientChoice();
            Action choosenMenuAction = this.menuActions.GetMethod(choice);
            Console.WriteLine("");
            choosenMenuAction();
        }

        public bool IsClientExit()
        {
            return clientExit;
        }

        public void Menu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(UIConsts.MENU);
        }

        public static void PrintExitMessage()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(UIConsts.EXIT_MESSAGE);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void PrintError(String message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Green;
        }

        private void InitalUIClientActions()
        {
            this.menuActions.Factory = new Dictionary<string, Action>()
            {
                { OptionsConsts.PRINT_ALL_CARS, this.uiPrintActions.PrintAllCars },
                { OptionsConsts.PRINT_CARS_BY_MODEL,this.uiPrintActions.PrintCarsByModelName },
                { OptionsConsts.PRINT_CAR_BY_INDEX, this.uiPrintActions.PrintCarByIndex},
                { OptionsConsts.PRINT_CARS_EQUALS_TO_DEFAULT, this.uiPrintActions.PrintEqualsToDefault },
                { OptionsConsts.PRINT_DEFAULT_CAR, this.uiPrintActions.PrintDefaultCar },

                { OptionsConsts.SET_DEAULT_CAR_BY_INDEX, this.uiDefualtActions.SetDefaultCarByIndex},
                { OptionsConsts.SET_DEAULT_CAR_BY_NUMBER, this.uiDefualtActions.SetDefaultCarByNumber},

                {OptionsConsts.BOOST_SPEED_BY_ONE, this.uiSpeedActions.BoostSpeedByOne },
                {OptionsConsts.BOOST_SPEED_BY_CONSTANT, this.uiSpeedActions.BoostSpeedByConstant },
                { OptionsConsts.DECREASE_SPEED_BY_ONE, this.uiSpeedActions.DecreaseSpeedByOne },
                { OptionsConsts.DECREASE_SPEED_BY_CONSTANT, this.uiSpeedActions.DecreaseSpeedByConstant },

                { OptionsConsts.ADD_CAR, this.uiAddActions.AddCar },
                { OptionsConsts.DELETE_DEFAULT_CAR, this.uiDeleteActions.DeleteDefualtCar },
                { OptionsConsts.DELETE_CAR_BY_NUMBER, this.uiDeleteActions.DeleteCarByNumber },


                { OptionsConsts.EXIT_OPTION,  this.ClientExiting}
            };
        }

        private String GetClientChoice()
        {
            Console.Write(InputConsts.ENETR_CHOICE_MESSAGE);
            String choice = Console.ReadLine();
            if (!this.menuActions.Factory.Keys.Contains(choice))
            {
                throw new ChoiceException();
            }
            return choice;
        }

        private void ClearScrean()
        {
            Console.WriteLine(UIConsts.PARSE_ANY_KEY);
            Console.ReadLine();
            Console.Clear();
        }

        // ****************** Client Exiting ******************
        private void ClientExiting()
        {
            this.clientExit = true;
        }
    }
}
