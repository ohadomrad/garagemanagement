﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Exceptions;

namespace GarageManagement.BL.Factory
{
    public class ActionsFactory : IMethodsFactory<String, Action>
    {
        Dictionary<String, Action> factory = new Dictionary<string, Action>();

        public Dictionary<String, Action> Factory
        {
            get { return this.factory; }
            set { this.factory = value; }
        }

        public void InsertMethod(String key, Action method)
        {
            if (!this.factory.ContainsKey(key))
                this.factory.Add(key, method);
        }

        public Action GetMethod(String key)
        {
            if (this.factory.ContainsKey(key))
            {
                Action optionFunction;
                this.factory.TryGetValue(key, out optionFunction);
                return optionFunction;
            }
            throw new KeyException();
        }
    }
}
