﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Factory
{
    public interface IMethodsFactory<K,V>
    {
        void InsertMethod(K key, V method);
        V GetMethod(K key);
        Dictionary<K, V> Factory { get; set; }
    }
}
