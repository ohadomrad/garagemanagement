﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Consts
{
    public class InputConsts
    {
        public static readonly String ENETR_CHOICE_MESSAGE = "Your choice: ";

        public static readonly String ENTER_CAR_INDEX_MESSAGE = "Enter index: ";

        public static readonly String ENTER_CAR_NUMBER_MESSAGE = "Enter car number: ";

        public static readonly String ENTER_BOOST_SPEED = "Enter the boost speed: ";

        public static readonly String ENTER_DECREASE_SPEED = "\nEnter the decrease speed: ";

        public static readonly String ENTER_CAR_MODEL_MESSAGE = "Enter car model: ";
    }
}
