﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Consts
{
    public class CarConsts
    {
        public static readonly int DEFAULT_INDEX = 0;
        public static readonly int MIN_INDEX = 0;
        public static readonly int ZERO = 0;


        public static readonly int BOTTOM_BLOCK_YEAR = 1886;

        public static readonly String DIGITS = "0123456789";
        public static readonly String TO_STRING_FORMAT = 
            "NumCar: {0} , manufactureYear: {1} , carModel {2} , Speed {3}";
    }
}
