﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Consts
{
    public class OptionsConsts
    {
        public static readonly String PRINT_ALL_CARS = "1";
        public static readonly String PRINT_CAR_BY_INDEX = "2";
        public static readonly String PRINT_DEFAULT_CAR = "3";
        public static readonly String PRINT_CARS_BY_MODEL = "4";
        public static readonly String PRINT_CARS_EQUALS_TO_DEFAULT = "5";

        public static readonly String SET_DEAULT_CAR_BY_INDEX = "6";
        public static readonly String SET_DEAULT_CAR_BY_NUMBER = "7";


        public static readonly String BOOST_SPEED_BY_ONE = "8";
        public static readonly String BOOST_SPEED_BY_CONSTANT = "9";

        public static readonly String DECREASE_SPEED_BY_ONE = "10";
        public static readonly String DECREASE_SPEED_BY_CONSTANT = "11";

        public static readonly String ADD_CAR = "12";

        public static readonly String DELETE_DEFAULT_CAR = "13";
        public static readonly String DELETE_CAR_BY_NUMBER = "14";

        public static readonly String EXIT_OPTION = "15";
    }
}
