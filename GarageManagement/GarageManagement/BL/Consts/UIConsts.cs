﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Consts
{
    public class UIConsts
    {
        public static readonly String MENU =  "**************************** GARAGE UI *************************\n"
                                            + "*                                                              *\n"
                                            + "* PRINT PRODUCTS                                               *\n"
                                            + "*    Enter 1 -> PRINT ALL CARS                                 *\n"
                                            + "*    Enter 2 -> Print CAR BY INDEX                             *\n"
                                            + "*    Enter 3 -> Print DEFAULT CAR                              *\n"
                                            + "*    Enter 4 -> PRINT CARS WITH THE SAME MODEL                 *\n"
                                            + "*    Enter 5 -> PRINT CARS EQUALS TO DEFAULT                   *\n"
                                            + "*                                                              *\n"
                                            + "* SET DEFAULT CAR                                              *\n"
                                            + "*    Enter 6 -> SET DEFAULT CAR BY INDEX                       *\n"
                                            + "*    Enter 7 -> SET DEFAULT CAR BY CAR NUMBER                  *\n"
                                            + "*                                                              *\n"
                                            + "* COMMON MERHODS                                               *\n"
                                            + "*    Enter 8 -> BOOST SPEED BY ONE                             *\n"
                                            + "*    Enter 9 -> BOOST SPEED BY CONSTANT                        *\n"
                                            + "*    Enter 10 -> DECREASE SPEED BY ONE                         *\n"
                                            + "*    Enter 11 -> DECREASE SPEED BY CONSTANT                    *\n"
                                            + "*                                                              *\n"
                                            + "* ADD PRODUCT                                                  *\n"
                                            + "*    Enter 12 -> ADD CAR                                       *\n"
                                            + "*                                                              *\n"
                                            + "* DELETE PRODUCT                                               *\n"
                                            + "*    Enter 13 -> DELETE DEFAULT CAR                            *\n"
                                            + "*    Enter 14 -> DELETE CAR BY NUMBER                          *\n"
                                            + "*                                                              *\n"
                                            + "* Enter 15 -> To Exit                                          *\n"
                                            + "*                                                              *\n"
                                            + "****************************************************************\n";

        public static readonly String EXIT_MESSAGE = "Bye Bye";

        public static readonly String CAR_INDEX_MESSAGE_FORMAT = "Car at index {0}";

        public static readonly String CAR_ADDED_MESSAGE = "[INFO] New car added";
        public static readonly String DELETE_CAR_MESSAGE = "[INFO] Car deleted";

        public static readonly String CAR_BOOST_MESSAGE = "[INFO] Car speed increased";
        public static readonly String CAR_DECREASE_MESSAGE = "[INFO] Car speed decresed";

        public static readonly String PARSE_ANY_KEY = "Parse any key to continue...";
    }
}
