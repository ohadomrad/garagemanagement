﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Consts
{
    public class ExceptionConsts
    {
        public static readonly String SPEED_ERROR = "[ERROR] Speed is bigger than zero";
        public static readonly String CONSTANT_SPEED_ERROR = "[ERROR] Speed Constant must be positive";

        public static readonly String NUM_CAR_ERROR = "[ERROR] Car number contains only digits";
        public static readonly String NUM_CAR_NOT_EXIST_ERROR = "[ERROR] Car number not exists";

        public static readonly String DUPLICATE_CAR_ERROR = "[ERROR] Car number allready exsists";
        public static readonly String MANUFACTURE_YEAR_ERROR = "[ERROR] Manufacture year not valid ";

        public static readonly String CAR_INDEX_ERROR = "[ERROR] Car index out of bounds ";

        public static readonly String KEY_ERROR = "[ERROR] Key Not Found";
        public static readonly String CHOICE_ERROR = "[ERROR] choose an option between [1-15]";
        public static readonly String INVALID_INPUT = "[ERROR] Invalid Input";


        public static readonly String EMPTY_GARAGE_ERROR = "Empty Garage";

        public static readonly String INT_ERROR = "[Error] Not and integer";

        public static readonly String INDEX_INPUT_ERROR = "[ERROR] Index must be an integer";
        public static readonly String SPEED_INPUT_ERROR = "[ERROR] Speed must be a number";



    }
}
