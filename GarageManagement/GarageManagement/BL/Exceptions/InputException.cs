﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Exceptions
{
    public class InputException: Exception
    {
        private String message;
        
        public InputException(String message)
        {
            this.message = message;
        }
        public override String ToString()
        {
            return message;
        }
    }
}
