﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Consts;

namespace GarageManagement.BL.Exceptions
{
    class KeyException : Exception
    {
        public override String ToString()
        {
            return ExceptionConsts.KEY_ERROR;
        }
    }
}
