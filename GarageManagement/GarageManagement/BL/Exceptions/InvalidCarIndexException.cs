﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Consts;

namespace GarageManagement.BL.Exceptions
{
    public class InvalidCarIndexException :Exception
    {
        public override String ToString()
        {
            return ExceptionConsts.CAR_INDEX_ERROR;
        }
    }
}
