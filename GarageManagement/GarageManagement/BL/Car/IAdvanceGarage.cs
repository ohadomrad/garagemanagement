﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Car
{
    public interface IAdvanceGarage :IGarage
    {
        void DeleteCarByIndex(int index);
        int Index { get; set; }

        IActiveCar GetCarByIndex(int index);
        void SetDefaultCar(int index);
        void SetDefaultCar(String carNum);
        void DeleteDefaultCar();

        IActiveCar GetDefaultCar();

        List<IActiveCar> GetEqualsToDefault();
    }
}
