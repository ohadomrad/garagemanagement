﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Car
{
    public interface ICar
    {
        String CarNumber { get; }
        int ManufactureYear { get; }
        String CarModel { get; }
        double Speed { get; set; }
    }
}
