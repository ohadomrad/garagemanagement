﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Car
{
    public interface IActiveCar : ICar
    {
        void BoostSpeed();
        void BoostSpeed(double boostSpeed);

        void DecreaseSpeed();
        void DecreaseSpeed(double decreaseSpeed);
    }
}
