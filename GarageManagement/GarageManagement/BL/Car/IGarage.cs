﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageManagement.BL.Car
{
    public interface IGarage
    {
        List<IActiveCar> Cars { get; }

        void AddCar(Car car);
        void DelteCarByNumber(String number);
        void GetCarsByModelName(String carModel);

        IActiveCar GetCarByCarNumber(String carNumber);
    }
}
