﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Exceptions;
using GarageManagement.BL.Consts;

namespace GarageManagement.BL.Car
{
    public class Garage :IAdvanceGarage
    {
        private List<IActiveCar> cars = new List<IActiveCar>();
        private int index = CarConsts.DEFAULT_INDEX;

        public int Index
        {
            get { return this.index; }
            set
            {
                
                if (index >= CarConsts.MIN_INDEX && index < this.cars.Count)
                {
                    this.index = value;
                }

                else if(this.cars.Count == CarConsts.ZERO)
                {
                    throw new EmptyGarageException();
                }

                else
                {
                    throw new InvalidCarIndexException();
                }
            }
        }
        public List<IActiveCar> Cars
        {
            get { return this.cars; }
        }

        public static IActiveCar CreateCar(String numCar, String carModel)
        {
            return new Car(numCar, DateTime.Now.Year, carModel);
        }

        public static void InsertToGarage(IGarage garage, IActiveCar car)
        {
            garage.Cars.Add(car);
        }

        public static List<IActiveCar> GetCarsByModelName(IGarage garage, String carModel)
        {
            return garage.Cars.FindAll(car => car.CarModel.Equals(carModel));
        }

        public IActiveCar this[int i]
        {
            get => this.cars.ElementAt(i);
            set => this.cars.Insert(i, value);
        }

        // ************************************ ADD ************************************


        public void AddCar(Car car)
        {
            Garage.InsertToGarage(this, car);
        }

        // ************************************ DEFAULT ************************************
        public void SetDefaultCar(int index)
        {
            SetIndex(index);
        }

        public void SetDefaultCar(String carNum)
        {
            int index = this.cars.FindIndex((car) => car.CarNumber.Equals(carNum));
            if (index == -1)
            {
                throw new CarNumberNotExistsException();
            }

            SetIndex(index);
        }


        // ************************************ DELETE ************************************

        public void DelteCarByNumber(String number)
        {
            IActiveCar c = this.cars.Find((car) => car.CarNumber.Equals(number));
            if( c == null)
            {
                throw new CarNumberNotExistsException();
            }

            DeleteCar(c);

        }
        public void DeleteDefaultCar()
        {
            this.DeleteCarByIndex(this.index);
        }

        public void DeleteCarByIndex(int index)
        {
            IActiveCar car = this.GetCarByIndex(index);
            DeleteCar(car);
        }

        // ************************************ GET CARS ************************************
        public List<IActiveCar> GetEqualsToDefault()
        {
            IActiveCar defaultCar = GetDefaultCar();
            List<IActiveCar> equalsCars = this.cars.FindAll((car) => (Car)car == (Car)defaultCar);
            return equalsCars;
        }
        public void GetCarsByModelName(string carModel)
        {
            Garage.GetCarsByModelName(this, carModel);
        }

        public IActiveCar GetDefaultCar()
        {
            if (this.cars.Count == CarConsts.ZERO)
            {
                throw new EmptyGarageException();
            }

            return GetCarByIndex(this.index);
        }

        public IActiveCar GetCarByIndex(int index)
        {
            if(this.cars.Count == CarConsts.ZERO)
            {
                throw new EmptyGarageException();
            }
            if (index < CarConsts.MIN_INDEX || index >= this.cars.Count)
            {
                throw new InvalidCarIndexException();
            }

            return this[index];
        }

        public IActiveCar GetCarByCarNumber(String carNumber)
        {
            IActiveCar c = this.cars.Find((car) => car.CarNumber.Equals(carNumber));
            if (c == null)
            {
                throw new CarNumberNotExistsException();
            }

            return c;
        }

        // ************************************ PRIVATE ************************************

        private void SetIndex(int index)
        {
            if(this.cars.Count == CarConsts.ZERO)
            {
                throw new EmptyGarageException();
            }

            if (index >= CarConsts.MIN_INDEX && index < this.cars.Count)
            {
                this.index = index;
            }

            else
            {
                throw new InvalidCarIndexException();
            }
        }
        private void DeleteCar(IActiveCar c)
        {
            IActiveCar carInGarage = GetCarByCarNumber(c.CarNumber);

            this.cars.RemoveAll((car) => car.CarNumber.Equals(carInGarage.CarNumber));          
        }
    }
}
