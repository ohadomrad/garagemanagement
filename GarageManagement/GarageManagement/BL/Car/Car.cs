﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GarageManagement.BL.Exceptions;
using GarageManagement.BL.Consts;


namespace GarageManagement.BL.Car
{
    public class Car : IActiveCar
    {
        private static String DIGITS = CarConsts.DIGITS;
        private static List<String> carsNumbers = new List<String>();
        private String numCar;
        private int manufactureYear;
        private String carModel;
        private double speed;

        public String CarNumber
        {
            get { return numCar; }
        }
        public int ManufactureYear
        {
            get { return manufactureYear; }
        }
        public String CarModel
        {
            get { return carModel; }
        }
        public double Speed
        {
            get { return this.speed; }
            set
            {
                if (value > 0)
                {
                    this.speed = value;
                }
                else
                {
                    throw new SpeedException();
                }
            }
        }

        public Car(IActiveCar car)
        {
            this.numCar = car.CarNumber;
            this.manufactureYear = car.ManufactureYear;
            this.carModel = car.CarModel;
        }

        public Car(String numCar, int manufactureYear, String carModel)
        {
            if (!IsNumCarValid(numCar))
            {
                throw new NumCarException();
            }

            if(Car.carsNumbers.Contains(numCar))
            {
                throw new DuplicateCarException();
            }

            this.numCar = numCar;
            Car.carsNumbers.Add(numCar);

            if(manufactureYear < CarConsts.BOTTOM_BLOCK_YEAR || manufactureYear > DateTime.Now.Year)
            {
                throw new ManufactureYearException();
            }

            this.manufactureYear = manufactureYear;
            this.carModel = carModel;
        }

        public override bool Equals(Object obj)
        {
            if(obj is ICar)
                return this.speed == ((ICar)obj).Speed;
            return false;
        }
        public static bool operator ==(Car car1, Car car2)
        {
            return car1.Equals(car2);
        }

        public static bool operator !=(Car car1, Car car2)
        {
            return !car1.Equals(car2);
        }

        public static Car operator +(Car car, double boostSpeed)
        {
            Car newCar = new Car(car);
            newCar.Speed += boostSpeed;
            return newCar;
        }

        public static Car operator -(Car car, double decreaseSpeed)
        {
            Car newCar = new Car(car);
            newCar.Speed -= decreaseSpeed;
            return newCar;
        }

        public override String ToString()
        {
            return String.Format(CarConsts.TO_STRING_FORMAT, numCar, manufactureYear, carModel, speed);
        }

        public void BoostSpeed()
        {   
            this.speed +=1;
        }

        public void BoostSpeed(double boostSpeed)
        {
            if(boostSpeed < 0)
            {
                throw new SpeedConstantException();
            }
            this.speed += boostSpeed;  
        }

        public void DecreaseSpeed()
        {
            if (this.speed == 0)
            {
                throw new SpeedException();
            }

            this.speed--;
        }

        public void DecreaseSpeed(double decreaseSpeed)
        {
            if (decreaseSpeed < 0)
            {
                throw new SpeedConstantException();
            }

            if (this.speed-decreaseSpeed < 0)
            {
                throw new SpeedException();
            }

            this.speed -= decreaseSpeed;
        }

        private bool IsNumCarValid(String numCar)
        {
            foreach (char c in numCar)
            {
                if (!Car.DIGITS.Contains(c))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
